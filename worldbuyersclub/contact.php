<?php
/**
 * Created by PhpStorm.
 * User: junjun
 * Date: 2016/6/7
 * Time: 10:56
 */
include ("header.php");?>

<div class="header-banner">
    <h2>Contact us</h2>
</div>
<div class="content">
    <div class="contact">
        <div class="contact-map">
            <iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJf4Zejjb4DogRs84n1HHMLZ4&key=AIzaSyCjjEFUzg9b53WqY4yP31FO_JQYgz2pJzo" width="100%" height="151px" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="contact_top">

            <div class="col-md-8 contact_left">
                <h3>Contact Form</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt dolor et tristique bibendum. Aenean sollicitudin vitae dolor ut posuere.</p>
                <form>
                    <div class="form_details">
                        <input type="text" class="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                        <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
                        <input type="text" class="text" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
                        <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                        <div class="clearfix"> </div>
                        <div class="sub-button">
                            <input type="submit" value="Send message">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 company-right">
                <div class="company_ad">
                    <h3>Contact Info</h3>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit velit justo.</span>
                    <address>
                        <p>Email:<a href="mailto:example@mail.com">example@mail.com</a></p>
                        <p>Phone: 1.306.222.4545</p>
                        <p>222 2nd Ave South</p>
                        <p>Saskabush, SK   S7M 1T6</p>

                    </address>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

<?php
include ("footer.php");
?>
