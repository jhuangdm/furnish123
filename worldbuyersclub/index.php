<?php
/**
 * Created by PhpStorm.
 * User: junjun
 * Date: 2016/6/7
 * Time: 9:55
 */

include ("header.php");?>

<div class="fluid_container">
    <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
        <div data-thumb="images/thumbs/slider1.jpg" data-src="images/slider1.jpg"></div>
        <div data-thumb="images/thumbs/slider2.jpg" data-src="images/slider2.jpg"></div>
        <div data-thumb="images/thumbs/slider3.jpg" data-src="images/slider5.jpg"></div>
        <div data-thumb="images/thumbs/slider4.jpg" data-src="images/slider4.jpg"></div>
    </div>
    <!-- #camera_wrap_1 -->
    <div class="clearfix"></div>
</div>
<div class="copyrights">Collect from <a href="http://www.cssmoban.com/" >企业网站模板</a></div>
<div class="interior-grids">
    <div class="col-md-4 interior-grid">
        <h3>welcome</h3>
        <div class="gallery">
            <a href="images/pic1.jpg"><img src="images/pic1.jpg" class="img-responsive" alt="/" title="image-name"></a>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.</p>
        <div class="plus_btn">
            <a href="#"><span></span></a>
        </div>
    </div>
    <div class="col-md-4 interior-grid">
        <h3>our history</h3>
        <div class="gallery">
            <a href="images/pic2.jpg"><img src="images/pic2.jpg" class="img-responsive" alt="/" title="image-name"></a>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.</p>
        <div class="plus_btn">
            <a href="#"><span></span></a>
        </div>
    </div>
    <div class="col-md-4 interior-grid">
        <h3>our services</h3>
        <div class="gallery">
            <a href="images/pic3.jpg"><img src="images/pic3.jpg" class="img-responsive" alt="/" title="image-name"></a>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.</p>
        <div class="plus_btn">
            <a href="#"><span></span></a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="bottom-grids">
    <div class="col-md-4 bottom-grid">
        <h3>Lorem Ipsum dolor</h3>
        <div class="text1-nav">
            <ul>
                <li><a href="#">The standard chunk of Lorem Ipsum</a></li>
                <li><a href="#">The standard chunk of Lorem Ipsum</a></li>
                <li><a href="#">The standard chunk of Lorem Ipsum</a></li>
                <li><a href="#">The standard chunk of Lorem Ipsum</a></li>
                <li><a href="#">The standard chunk of Lorem Ipsum</a></li>

            </ul>
        </div>
    </div>
    <div class="col-md-4 bottom-grid">
        <h3>Vestibulum vel</h3>
        <h4>The standard chunk of Lorem Ipsum dolor sit amet</h4>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage</p>
    </div>
    <div class="col-md-4 bottom-grid">
        <h3>Quisque non ligula</h3>
        <h4>The standard chunk of Lorem Ipsum dolor sit amet</h4>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage</p>
    </div>
    <div class="clearfix"></div>
</div>
<div class="product-section">
    <h3>furniture</h3>
    <div class="product-grids">
        <div class="col-md-3 product-grid">
            <a href="#" class="mask">
                <img src="images/e1.jpg" class="img-responsive zoom-img" alt="">
            </a>
        </div>
        <div class="col-md-3 product-grid1">
            <h4><a href="#">Mauris non magna ul</a></h4>
            <p>Fusce feugiat malesuada odio. Morbi orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna.</p>
            <a href="#" class="button1">more</a>
        </div>
        <div class="col-md-3 product-grid">
            <a href="#" class="mask">
                <img src="images/e2.jpg" class="img-responsive zoom-img" alt="">
            </a>
        </div>
        <div class="col-md-3 product-grid1">
            <h4><a href="#">Mauris non magna ul</a> </h4>
            <p>Fusce feugiat malesuada odio. Morbi orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna.</p>
            <a href="#" class="button1">more</a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="new-section">
    <div class="new-grids">
        <div class="col-md-6 new-grid">
            <div class="new-number">
                <h4>09</h4>
            </div>
            <div class="new-text">
                <h5>Lorem Ipsum is simply text </h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-6 new-grid">
            <div class="new-number">
                <h4>19</h4>
            </div>
            <div class="new-text">
                <h5>Lorem Ipsum is simply text </h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<?php
include ("footer.php");
?>
