<!DOCTYPE HTML>
<html>
<head>
    <title>Home</title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Inland Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="css/camera.css" rel="stylesheet" type="text/css" media="all" />
    <script type='text/javascript' src='js/jquery.min.js'></script>
    <script type='text/javascript' src='js/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='js/jquery.easing.1.3.js'></script>
    <script type='text/javascript' src='js/camera.min.js'></script>

    <script>
        jQuery(function(){

            jQuery('#camera_wrap_1').camera({
                thumbnails: true
            });

            jQuery('#camera_wrap_2').camera({
                height: '400px',
                loader: 'bar',
                pagination: false,
                thumbnails: true
            });
        });
    </script>
    <!--start lightbox -->
    <link rel="stylesheet" type="text/css" href="css/jquery.lightbox.css">
    <script src="js/jquery.lightbox.js"></script>
    <script>
        // Initiate Lightbox
        $(function() {
            $('.gallery a').lightbox();
        });
    </script>

</head>
<body>
<div class="container">
    <div class="main-header">
        <div class="inner-side">
            <div class="header">
                <div class="head-top">
                    <div class="top-menu">
                        <span class="menu"><img src="images/nav.png" alt=""/> </span>
                        <ul>
                            <li class="active"><a href="index.html"><span>Home</span></a></li>
                            <li><a href="about.html"><span>About us</span></a></li>
                            <li><a href="products.html"><span>Products</span></a></li>
                            <li class="has-sub"><a href="services.html"><span>Services</span></a></li>
                            <li class="last"><a href="contact.html"><span>Contact</span></a></li>
                            <li class="last"><a href="club.html"><span>Join Club</span></a></li>
                        </ul>
                    </div>
                    <!-- script for menu -->

                    <script>
                        $("span.menu").click(function(){
                            $(".top-menu ul").slideToggle("slow" , function(){
                            });
                        });
                    </script>
                    <!-- //script for menu -->

                    <div class="clearfix"></div>
                </div>
                <div class="logo">
                    <h1><a href="index.html"><span>WorldBuyers</span>club</a></h1>
                </div>
            </div>